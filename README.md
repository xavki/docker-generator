
# Generator


Lancement de briques pour tests en tout genre (voir intégration dans des pipelines pour test)


Options:

  -h, --help
    can I help you ?

  -c, --cassandra
    run cassandra container

  -i, --ip
    list ip for each container

  -p, --prometheus
    run prometheus container

  -gra, --grafana
    run grafana container

  -j, --jenkins
    run jenkins container

  -m, --mariadb
    run mariadb

  -gil, --gitlab
    run gitlab

